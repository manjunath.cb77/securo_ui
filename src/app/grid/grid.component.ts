import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { CommonService } from '../common.service';
import { ColDef } from 'ag-grid-community';
import { DeleteIconComponent } from '../delete-icon/delete-icon.component';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit, OnChanges {


  gridcoloum:any;
 defgrid:any;
 getlist:any;
 userdata:any;
  //  userdata:any=[{
  //   name:"manju",
  //   size:1542,
  //   time:1253.6284,
  //   type:"pdf"
  //  }
  // ]


  constructor(private comm:CommonService){}
  ngOnInit(): void {
    this.getdata();
  }

  ngOnChanges(changes: SimpleChanges): void {
     this.getdata();
  }

  public getdata(){
    // this.comm.getfiledata().subscribe((event:any)=>{
    //   this.userdata=event
    // })
    
      this.userdata=this.comm.getfiledata();
    
    
    console.log(this.userdata)
  }

  public columnDefs: ColDef[] = [
    { field: 'name'},
    { field: 'size'},
    { field: 'time' },
    { field: 'type' },
    { field: 'more', cellRenderer:DeleteIconComponent ,
    minWidth:100,
    cellRendererParams:{
      delete:true
    }
  }
  ]
  public defaultColDef: ColDef = {
    editable: true,
    enableRowGroup: true,
    enablePivot: true,
    enableValue: true,
    sortable: true,
    resizable: true,
    filter: true,
    flex: 1,
    minWidth: 100,
  };


}

import { Component, OnChanges, OnInit } from '@angular/core';
import { CommonService } from '../common.service';
interface UploadedFile {
  name: string;
  size: number;
  time: Date;
  type: string;
}
@Component({
  selector: 'app-fileupload',
  templateUrl: './fileupload.component.html',
  styleUrls: ['./fileupload.component.css']
})
export class FileuploadComponent implements OnInit , OnChanges{
  isLogged=true;

  filesToUpload: UploadedFile[] = [];
  constructor(private comm:CommonService){}
  ngOnInit(): void {
    this.loginstatus();
  }
  ngOnChanges(){
    this.loginstatus();
  }
 
  public loginstatus(){
    this.isLogged=this.comm.getloginstatus();
  }

  onFileSelected(event: any) {
    const selectedFiles: FileList = event.target.files;

    for (let i = 0; i < selectedFiles.length; i++) {
      const file: File = selectedFiles[i];
      if (file.size <= 2 * 1024 * 1024) { 
        const uploadedFile: UploadedFile = {
          name: file.name,
          size: file.size,
          time: new Date(),
          type: file.type
        };
        this.filesToUpload.push(uploadedFile);
        console.log(uploadedFile);
      } else {
        console.log('File size exceeds the limit 2 MB');
      }
    }
  }

  onUpload() {
    if (this.filesToUpload.length > 0) {
      let totalSize = 0;

      for (let i = 0; i < this.filesToUpload.length; i++) {
        totalSize += this.filesToUpload[i].size;
      }

      if (totalSize <= 10 * 1024 * 1024) { 
        
        this.comm.sendfiledata(this.filesToUpload);
        console.log(this.filesToUpload)
      } else {
        console.log('Total file size exceeds the limit (10 MB)');
      }

      
    }
  }

}

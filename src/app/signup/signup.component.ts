import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CommonService } from '../common.service';
import {  MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent {

  UserForm:any;

constructor(private comm:CommonService, private dialog:MatDialogRef<SignupComponent>){}

  ngOnInit(): void{
    this.openform();
  
  }
  public openform(){
      this.UserForm  =  new FormGroup({
             name:new FormControl('',Validators.required),
            username:new FormControl('',Validators.required),
             password:new FormControl('',Validators.required)
       });
      }
  
    public savefrom(){
        this.comm.saveemployee(this.UserForm.value);
        console.log(this.UserForm.value)
           this.dialog.close();
       }
  public oncancel(){
    this.dialog.close();
  }
          
  
}

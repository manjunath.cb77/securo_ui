import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { ICellRendererParams } from 'ag-grid-community';

@Component({
  selector: 'app-delete-icon',
  templateUrl: './delete-icon.component.html',
  styleUrls: ['./delete-icon.component.css']
})
export class DeleteIconComponent implements ICellRendererAngularComp {
  [x: string]: any;
  
  
  value:any;
  isDeleteEnabled=false;
     refresh(params:any): boolean {
      this.value=params.value;
      return true;
    }
  
    agInit(params: ICellRendererParams<any, any>): void {
      console.log(params);
      if(params && params['colDef']){
        let gridParams=params['colDef']['cellRendererParams'];
        this.isDeleteEnabled=gridParams['delete'];
      }
    }
  }

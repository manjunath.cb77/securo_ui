import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common.service';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { SignupComponent } from '../signup/signup.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 isLogged=false;
  UserForm:any;
  
constructor(private comm:CommonService,private dialog:MatDialog){}


  ngOnInit(): void {
    this.getloggingsatatus();
    this.openform();
  }
  openform() {
    this.UserForm  =  new FormGroup({
      username:new FormControl(),
      password:new FormControl()
  });
  }

  public  openuserform()  { 
  const dialog=this.dialog.open(SignupComponent);
  dialog.afterClosed();
  }




 public getloggingsatatus(){
    this.isLogged=this.comm.getloginstatus();
  }
 

public login(){

  

this.comm.getemployee(this.UserForm.value);

}


public logout(){
  this.comm.LoggedOut();
}



}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FileuploadComponent } from './fileupload/fileupload.component';
import { GridComponent } from './grid/grid.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {
    path:'fileupload',
    component:FileuploadComponent
  },
  {
    path:'grid',
    component:GridComponent
  },
  {
    path:'login',
    component:LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

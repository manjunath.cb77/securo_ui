import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  user:any;
  islogged=false;
  userlist:any=[]
  filedata:any;
 
  constructor(private http:HttpClient,private router:Router) { }
 
public saveemployee(data: any){
  console.log(data);
  this.userlist.push(data)

 //this.http.post("http://localhost:8081/user/saveuser",data);
}


public getemployee(data:any){
 //this.user= this.http.get("http://localhost:8081/user/getuser",data).subscribe();
  let i;
 for( i=0;i<=this.userlist.length;i++){
      this.user=this.userlist[i];
      console.log(this.user);
      if(this.user.username==data.username && this.user.password==data.password){
        this.islogged=true;
        this.router.navigateByUrl("/fileupload");
      }
 }

 
}



public  LoggedOut(){
  this.islogged=false;
}
getloginstatus(): boolean{
  return this.islogged;
  }


public sendfiledata(data:any){
  this.filedata=data;
  console.log(this.filedata)
  this.router.navigateByUrl("/grid");
 // this.http.post("http://localhost:8081/user/savefile",data);
}
public getfiledata(){
  return this.filedata;
 // return this.http.get("http://localhost:8081/user/getfiles");
}

  

}
